import numpy as np
import utilities
from solution import DecisionTree

def run_test_data_on_tree():
    dataset = utilities.load_csv('Student Data.csv')
    print("---------------")
    ten_folds = utilities.generate_k_folds(dataset, 10)
    print("---------------")
    accuracies = []
    precisions = []
    recalls = []
    confusion = []

    for fold in ten_folds:
        train, test = fold
        train_features, train_classes = train
        test_features, test_classes = test
        tree = DecisionTree()
        tree.fit(train_features, train_classes)
        output = tree.classify(test_features)
        accuracies.append(utilities.accuracy(output, test_classes))
        precisions.append(utilities.precision(output, test_classes))
        recalls.append(utilities.recall(output, test_classes))
        confusion.append(utilities.confusion_matrix(output, test_classes))

    print("tree average = " + str(np.average(accuracies)))

run_test_data_on_tree()
