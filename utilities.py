import math
import random
import numpy as np

def load_csv(data_file_path, class_index=-1):
    handle = open(data_file_path, 'r')
    contents = handle.read()
    handle.close()
    rows = contents.split('\n')
    out = np.array([[float(i) for i in r.split(',')] for r in rows if r ])
    classes= map(int,  out[:, class_index])
    features = out[:, :class_index]
    return features, classes

def generate_k_folds(dataset, k):
    # where each fold is a tuple like (training_set, test_set)
    # where each set is a tuple like (examples, classes)
    examples = list(zip(dataset[0], dataset[1]))
    print(examples[0])
    random.shuffle(examples)
    print(examples[0])
    folds = np.array_split(examples,k)
    # up to this point I have dataset split into k groups
    final_folds = []
    for i in range(len(folds)):
        training_examples = []
        training_classes = []
        for j in range(len(folds)):
            if i != j:
                for item in folds[j]:
                    training_examples.append(item[0])
                    training_classes.append(item[1])
        testing_examples = []
        testing_classes = []
        for item in folds[i]:
            testing_examples.append(item[0])
            testing_classes.append(item[1])
        final_folds.append(((training_examples,training_classes),(testing_examples,testing_classes)))
    return final_folds


def confusion_matrix(classifier_output, true_labels):
    # output should be [[true_positive, false_negative], [false_positive, true_negative]]
    true_positive = 0
    false_positive = 0
    true_negative = 0
    false_negative = 0
    for i in range(len(true_labels)):
        if classifier_output[i] == 1:
            if true_labels[i] == 1:
                true_positive += 1
            else:
                false_positive += 1
        else:
            if true_labels[i] == 1:
                false_negative += 1
            else:
                true_negative += 1

    return [[true_positive, false_negative], [false_positive, true_negative]]


def precision(classifier_output, true_labels):
    # precision is measured as: true_positive/ (true_positive + false_positive)
    con_mat = confusion_matrix(classifier_output, true_labels)
    if con_mat[0][0] + con_mat[1][0] != 0:
        return 1.0 * con_mat[0][0] / (con_mat[0][0] + con_mat[1][0])
    return 0


def recall(classifier_output, true_labels):
    # recall is measured as: true_positive/ (true_positive + false_negative)
    con_mat = confusion_matrix(classifier_output, true_labels)
    if con_mat[0][0] + con_mat[0][1] != 0:
        return 1.0 * con_mat[0][0] / (con_mat[0][0] + con_mat[0][1])
    return 0


def accuracy(classifier_output, true_labels):
    # accuracy is measured as:  correct_classifications / total_number_examples
    correct = 0.0
    vals = zip(classifier_output, true_labels)
    for val in vals:
        if val[0] == val[1]:
            correct += 1.0
    return correct / len(true_labels)
