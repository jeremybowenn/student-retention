import unittest
from solution import DecisionTree

class EntropyTest(unittest.TestCase):
    def test0(self):
        tree = DecisionTree()
        self.assertEqual(0, tree._entropy([0,0,0,0,0]))

    def test1(self):
        tree = DecisionTree()
        self.assertEqual(0, tree._entropy([1,1,1,1,1]))

    def test2(self):
        tree = DecisionTree()
        self.assertEqual(1, tree._entropy([0,0,0,1,1,1]))

    def test3(self):
        tree = DecisionTree()
        self.assertAlmostEqual(0.97095, tree._entropy([0,1,0,1,0]), delta=0.00001)