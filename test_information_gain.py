import unittest
from solution import DecisionTree

class InformationGainTest(unittest.TestCase):
    def test0(self):
        tree = DecisionTree()
        self.assertEqual(0, tree._information_gain([0,0,0,0,0], [[0,0],[0,0,0]]))

    def test1(self):
        tree = DecisionTree()
        self.assertAlmostEqual(0.0817, tree._information_gain([0, 0, 0, 1, 1, 1], [[0, 1, 0], [0, 0, 1]]), delta=0.00001)

    def test2(self):
        tree = DecisionTree()
        self.assertAlmostEqual(0.01997, tree._information_gain([0, 1,0,1,0], [[0, 1], [0, 1, 0]]), delta=0.00001)

    def test3(self):
        tree = DecisionTree()
        self.assertAlmostEqual(0.97095, tree._information_gain([0, 0, 1, 1, 1], [[0, 0], [1, 1, 1]]), delta=0.00001)