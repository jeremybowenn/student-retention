import unittest
from solution import DecisionTree

class BTest(unittest.TestCase):
    def test0(self):
        tree = DecisionTree()
        self.assertEqual(0, tree._B(0.0))

    def test1(self):
        tree = DecisionTree()
        self.assertEqual(0, tree._B(1.0))

    def test2(self):
        tree = DecisionTree()
        self.assertEqual(1, tree._B(0.5))

    def test3(self):
        tree = DecisionTree()
        self.assertAlmostEqual(0.81128, tree._B(0.25), delta=0.00001)
