import math
import random
import numpy as np

class DecisionNode():
    """Class to represent a single node in
    a decision tree."""

    def __init__(self, left, right, decision_function,class_label=None):
        """Create a node with a left child, right child,
        decision function and optional class label
        for leaf nodes."""
        self.left = left
        self.right = right
        self.decision_function = decision_function
        self.class_label = class_label

    def decide(self, feature):
        """Return on a label if node is leaf,
        or pass the decision down to the node's
        left/right child (depending on decision
        function)."""
        if self.class_label is not None:
            return self.class_label
        elif self.decision_function(feature):
            return self.left.decide(feature)
        else:
            return self.right.decide(feature)


#1.a
def build_decision_tree():
    """Create decision tree
    capable of handling the provided 
    data."""
    true_node = DecisionNode(None, None, None, 1)
    false_node = DecisionNode(None, None, None, 0)
    check_A2 = DecisionNode(true_node,false_node,lambda feature: feature[0] == 1)
    decision_tree_root = DecisionNode(true_node,check_A2,lambda  feature: feature[2] == feature[3])

    return decision_tree_root

#1.b
def confusion_matrix(classifier_output, true_labels):
    #output should be [[true_positive, false_negative], [false_positive, true_negative]]
    true_positive = 0
    false_positive = 0
    true_negative = 0
    false_negative = 0
    for i in range(len(true_labels)):
        if classifier_output[i] == 1:
            if true_labels[i] == 1:
                true_positive += 1
            else:
                false_positive += 1
        else:
            if true_labels[i] == 1:
                false_negative += 1
            else:
                true_negative += 1

    return [[true_positive, false_negative], [false_positive, true_negative]]

def precision(classifier_output, true_labels):
    #precision is measured as: true_positive/ (true_positive + false_positive)
    con_mat = confusion_matrix(classifier_output, true_labels)
    if con_mat[0][0]+ con_mat[1][0] != 0:
        return 1.0*con_mat[0][0]/ (con_mat[0][0]+ con_mat[1][0])
    return 0
    
def recall(classifier_output, true_labels):
    #recall is measured as: true_positive/ (true_positive + false_negative)
    con_mat = confusion_matrix(classifier_output, true_labels)
    if con_mat[0][0]+ con_mat[0][1] != 0:
        return 1.0*con_mat[0][0]/ (con_mat[0][0]+ con_mat[0][1])
    return 0
    
def accuracy(classifier_output, true_labels):
    #accuracy is measured as:  correct_classifications / total_number_examples
    correct = 0.0
    vals = zip(classifier_output, true_labels)
    for val in vals:
        if val[0] == val[1]:
            correct += 1.0
    return correct/len(true_labels)

def entropy(class_vector):
    """Compute the entropy for a list
    of classes (given as either 0 or 1)."""
    ratio = 1.0*sum(class_vector)/len(class_vector)
    return B(ratio)


def B(q):
    if q < 1 and q > 0:
        return -(q * math.log(q, 2) + (1 - q) * math.log(1 - q, 2))
    return 0.0

def information_gain(previous_classes, current_classes ):
    """Compute the information gain between the
    previous and current classes (each 
    a list of 0 and 1 values)."""
    ent = entropy(previous_classes)
    remainder = 0.0
    previous_classes = list(previous_classes)
    #print("current classes = "+str(list(current_classes)))
    #print("prev class = "+str(previous_classes))
    for c in current_classes:
        if len(c) != 0:
            remainder += 1.0 * len(c) / len(previous_classes) * B(1.0 * sum(c) / len(c))
    return ent - remainder

class DecisionTree():
    """Class for automatic tree-building
    and classification."""

    def __init__(self, depth_limit=float('inf')):
        """Create a decision tree with an empty root
        and the specified depth limit."""
        self.root = None
        self.depth_limit = depth_limit

    def fit(self, features, classes):
        """Build the tree from root using __build_tree__()."""
        self.root = self.__build_tree__(features, classes)

    def __build_tree__(self, features, classes, depth=0):  
        """Implement the above algorithm to build
        the decision tree using the given features and
        classes to build the decision functions."""
        #create simple tree
        if sum(classes) == 0:
            return DecisionNode(None, None, None, 0)
        if sum(classes)  / len(classes) > 0:
            return DecisionNode(None, None, None, 1)
        if depth == self.depth_limit:
            val = int(round(1.0 * sum(classes) / len(classes)))
            return DecisionNode(None, None, None, val)
        avg_array = np.average(features, axis=0)
        median_array = np.median(features, axis=0)
        stdev_array = np.std(features, axis=0)
        min_array = np.min(features, axis=0)
        shift_array = [a-2*s for a,s in zip(avg_array,stdev_array)]
        """
        #__get_gain_array__ results in
        0 = max_gain
        1 = feature
        2 = thresh_value
        3 = pos_features_set
        4 = pos_classes_set
        5 = neg_features_set
        6 = neg_classes_set
        """
        vals = []
        vals.append(self.__get_gain_array__(features, classes, avg_array))
        vals.append(self.__get_gain_array__(features, classes, median_array))
        vals.append(self.__get_gain_array__(features, classes, shift_array))
        vals.append(self.__get_gain_array__(features, classes, min_array))
        best_val = vals[0]
        for i in range(1,len(vals)):
            if vals[i][0] > best_val[0]:
                best_val = vals[i]
        #print("Depth = "+str(depth))
        true_node = self.__build_tree__(best_val[3],best_val[4], depth+1)
        false_node = self.__build_tree__(best_val[5],best_val[6],depth+1)
        #print("--"+str(best_val[1])+" and "+str(best_val[2]))
        f = lambda feature: feature[best_val[1]] >= best_val[2]

        return DecisionNode(true_node, false_node, f)

    def __get_gain_array__(self, features, previous_classes, thresh_array):
        max_gain = None
        gain_array = []
        feature = -1
        thresh_value = -1
        pos_classes_set = None
        pos_features_set = None
        neg_classes_set = None
        neg_features_set = None
        for i in range(len(thresh_array)):
            pos_classes = []
            pos_examples = []
            neg_classes = []
            neg_examples = []
            for example_index in range(len(features)):
                if features[example_index][i] >= thresh_array[i]:
                    pos_examples.append(features[example_index])
                    pos_classes.append(previous_classes[example_index])
                else:
                    neg_examples.append(features[example_index])
                    neg_classes.append(previous_classes[example_index])
            current_gain = information_gain(previous_classes, [pos_classes, neg_classes])
            #gain_array.append((current_gain,thresh_array[i],i,pos,neg))
            if max_gain is None or current_gain > max_gain:
                thresh_value = thresh_array[i]
                max_gain = current_gain
                pos_classes_set = pos_classes
                pos_features_set = pos_examples
                neg_classes_set = neg_classes
                neg_features_set = neg_examples
                feature = i

        return max_gain,feature, thresh_value,pos_features_set,pos_classes_set,neg_features_set,neg_classes_set

        #return gain_array



    def classify(self, features):
        """Use the fitted tree to 
        classify a list of examples. 
        Return a list of class labels."""
        class_labels = []
        class_labels = [self.root.decide(feature) for feature in features]

        return class_labels


#2.b
def load_csv(data_file_path, class_index=-1):
    handle = open(data_file_path, 'r')
    contents = handle.read()
    handle.close()
    rows = contents.split('\n')
    out = np.array([[float(i) for i in r.split(',')] for r in rows if r ])
    classes= map(int,  out[:, class_index])
    features = out[:, :class_index]
    return features, classes

def generate_k_folds(dataset, k):
    examples = (zip(dataset[0], dataset[1]))
    print(examples[0])
    random.shuffle(examples)
    folds = np.array_split(examples,k)
    # up to this point I have dataset split into k groups
    final_folds = []
    for i in range(len(folds)):
        training_examples = []
        training_classes = []
        for j in range(len(folds)):
            if i != j:
                for item in folds[j]:
                    training_examples.append(item[0])
                    training_classes.append(item[1])
        testing_examples = []
        testing_classes = []
        for item in folds[i]:
            testing_examples.append(item[0])
            testing_classes.append(item[1])
        final_folds.append(((training_examples,training_classes),(testing_examples,testing_classes)))
    return final_folds

#3
class RandomForest():
    """Class for random forest
    classification."""

    def __init__(self, num_trees, depth_limit, example_subsample_rate, attr_subsample_rate):
        """Create a random forest with a fixed 
        number of trees, depth limit, example
        sub-sample rate and attribute sub-sample
        rate."""
        self.trees = []
        self.num_trees = num_trees
        self.depth_limit = depth_limit
        self.example_subsample_rate = example_subsample_rate
        self.attr_subsample_rate = attr_subsample_rate

    def fit(self, features, classes):
        """Build a random forest of 
        decision trees."""
        examples = list(zip(features,classes))
        for i in range(self.num_trees):
            random.shuffle(examples)
            new_tree = DecisionTree(self.depth_limit)
            n = int(self.example_subsample_rate*len(examples))
            sub_set = examples[:n]
            my_features, my_classes = zip(*sub_set)
            my_classes = list(my_classes)
            my_classes = np.array(my_classes)
            my_features = list(my_features)
            my_features = np.array(my_features).T
            attr_cnt = int(self.attr_subsample_rate * my_features.shape[0])
            sel_list = [x for x in range(my_features.shape[0])]
            random.shuffle(sel_list)
            sub_features = []
            sel_list = sel_list[:attr_cnt]
            for x in sel_list:
                sub_features.append(my_features[x])
            sub_features = np.array(sub_features).T
            new_tree.fit(sub_features,my_classes)
            self.trees.append((new_tree,sel_list))

    def classify(self, features):
        """Classify a list of features based
        on the trained random forest."""
        #print("############################################")
        #print("features = "+str(features))
        class_labels = []
        results = []
        features = np.array(features).T
        for tree_tuple in self.trees:
            sub_features = []
            tree, sel_list = tree_tuple
            for x in sel_list:
                sub_features.append(features[x])
            sub_features = np.array(sub_features).T
            results.append(tree.classify(sub_features))
        results = np.array(results).T
        for item in results:
            class_labels.append(int(round(1.0 * sum(item) / len(item))))
        return class_labels


class ChallengeClassifier():
    
    def __init__(self, depth_limit = float("inf")):
        # initialize whatever parameters you may need here-
        # this method will be called without parameters 
        # so if you add any to make parameter sweeps easier, provide defaults
        self.root = None
        self.depth_limit = depth_limit
        
    def fit(self, features, classes):
        # fit your model to the provided features
        self.root = self.__build_tree__(features, classes)

    def __build_tree__(self, features, classes, depth=0):
        """Implement the above algorithm to build
        the decision tree using the given features and
        classes to build the decision functions."""
        #create simple tree
        if sum(classes) == 0:
            return DecisionNode(None, None, None, 0)
        if sum(classes)  / len(classes) > 0:
            return DecisionNode(None, None, None, 1)
        if depth == self.depth_limit:
            val = int(round(1.0 * sum(classes) / len(classes)))
            return DecisionNode(None, None, None, val)
        avg_array = np.average(features, axis=0)
        median_array = np.median(features, axis=0)
        stdev_array = np.std(features, axis=0)
        min_array = np.min(features, axis=0)
        shift_array = [a-2*s for a,s in zip(avg_array,stdev_array)]
        """
        #__get_gain_array__ results in
        0 = max_gain
        1 = feature
        2 = thresh_value
        3 = pos_features_set
        4 = pos_classes_set
        5 = neg_features_set
        6 = neg_classes_set
        """
        vals = []
        vals.append(self.__get_gain_array__(features, classes, avg_array))
        vals.append(self.__get_gain_array__(features, classes, median_array))
        vals.append(self.__get_gain_array__(features, classes, shift_array))
        vals.append(self.__get_gain_array__(features, classes, min_array))
        best_val = vals[0]
        for i in range(1,len(vals)):
            if vals[i][0] > best_val[0]:
                best_val = vals[i]
        #print("Depth = "+str(depth))
        true_node = self.__build_tree__(best_val[3],best_val[4], depth+1)
        false_node = self.__build_tree__(best_val[5],best_val[6],depth+1)
        #print("--"+str(best_val[1])+" and "+str(best_val[2]))
        f = lambda feature: feature[best_val[1]] >= best_val[2]

        return DecisionNode(true_node, false_node, f)

    def __get_gain_array__(self, features, previous_classes, thresh_array):
        max_gain = None
        gain_array = []
        feature = -1
        thresh_value = -1
        pos_classes_set = None
        pos_features_set = None
        neg_classes_set = None
        neg_features_set = None
        for i in range(len(thresh_array)):
            pos_classes = []
            pos_examples = []
            neg_classes = []
            neg_examples = []
            for example_index in range(len(features)):
                if features[example_index][i] >= thresh_array[i]:
                    pos_examples.append(features[example_index])
                    pos_classes.append(previous_classes[example_index])
                else:
                    neg_examples.append(features[example_index])
                    neg_classes.append(previous_classes[example_index])
            current_gain = information_gain(previous_classes, [pos_classes, neg_classes])
            #gain_array.append((current_gain,thresh_array[i],i,pos,neg))
            if max_gain is None or current_gain > max_gain:
                thresh_value = thresh_array[i]
                max_gain = current_gain
                pos_classes_set = pos_classes
                pos_features_set = pos_examples
                neg_classes_set = neg_classes
                neg_features_set = neg_examples
                feature = i

        return max_gain,feature, thresh_value,pos_features_set,pos_classes_set,neg_features_set,neg_classes_set

    def classify(self, features):
        # classify each feature in features as either 0 or 1.
        class_labels = [self.root.decide(feature) for feature in features]
        return class_labels