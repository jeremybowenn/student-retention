from utilities import *
from DecisionNode import DecisionNode


class DecisionTree:
    def __init__(self, depth_limit=4):
        # TODO: You may want to alter the default depth_limit to avoid a RecursionError
        """Create a decision tree with an empty root
        and the specified depth limit."""
        self.root = None
        self.depth_limit = depth_limit

    def fit(self, features, classes):
        """
        Build the tree from root using __build_tree__().
        """
        self.root = self.__build_tree__(features, classes)

    def __build_tree__(self, features, classes, depth=0):
        """
        Implement the above algorithm to build
        the decision tree using the given features and
        classes to build the decision functions.

        This is a recursive function that builds the tree. If the node is a leaf then it would have a statement such
        as 'return DecisionNode(None, None, None, 0)' if the label of the leaf was 0.  Note the label is the same as
        the classification.  If it is not a leaf then it needs a decision function that may look something like

        func_to_split_on = lambda feature: feature[feature_to_split_on] >= value_to_split_on

        This will create what is called a lambda function that can be passed as a variable.  So you can then say

        return DecisionNode(Node_on_True, Node_on_False, func_to_split_on)

        In this the Node_on_True and Node_on_False will need to have been created first (hence the recursion).

        :param features: The list of all the features that lead to a classification
        :param classes: The list of classifications.  This is a parallel list to the features list
        :param depth: In case a depth-limit was implemented this will keep track of the depth of the tree.
        :return: A DecisionNode.
        """
        # TODO: finish this
        if sum(classes) == 0:
            return DecisionNode(None, None, None, 0)
        # print(sum(classes) / len(classes))
        if sum(classes) / len(classes) < 0:
            return DecisionNode(None, None, None, 1)
        if depth == self.depth_limit:
            val = int(round(1.0 * sum(classes) / len(classes)))
            return DecisionNode(None, None, None, val)
        # print("Anything")
        avg_array = np.average(features, axis=0)
        median_array = np.median(features, axis=0)
        stdev_array = np.std(features, axis=0)
        min_array = np.min(features, axis=0)
        shift_array = [a-2*s for a, s in zip(avg_array, stdev_array)]
        """
        #__get_gain_array__ results in
        0 = max_gain
        1 = feature
        2 = thresh_value
        3 = pos_features_set
        4 = pos_classes_set
        5 = neg_features_set
        6 = neg_classes_set
        """
        vals = []
        vals.append(self.__get_gain_array__(features, classes, avg_array))
        vals.append(self.__get_gain_array__(features, classes, median_array))
        vals.append(self.__get_gain_array__(features, classes, shift_array))
        vals.append(self.__get_gain_array__(features, classes, min_array))
        best_val = vals[0]
        for i in range(1, len(vals)):
            if vals[i][0] > best_val[0]:
                best_val = vals[i]
        # print("Depth = "+str(depth))
        true_node = self.__build_tree__(best_val[3], best_val[4], depth+1)
        false_node = self.__build_tree__(best_val[5], best_val[6], depth+1)
        # print("--"+str(best_val[1])+" and "+str(best_val[2]))

        f = lambda feature: feature[best_val[1]] >= best_val[2]

        return DecisionNode(true_node, false_node, f)

    def __get_gain_array__(self, features, previous_classes, thresh_array):
        max_gain = None
        gain_array = []
        feature = -1
        thresh_value = -1
        pos_classes_set = None
        pos_features_set = None
        neg_classes_set = None
        neg_features_set = None
        for i in range(len(thresh_array)):
            pos_classes = []
            pos_examples = []
            neg_classes = []
            neg_examples = []
            for example_index in range(len(features)):
                if features[example_index][i] >= thresh_array[i]:
                    pos_examples.append(features[example_index])
                    pos_classes.append(previous_classes[example_index])
                else:
                    neg_examples.append(features[example_index])
                    neg_classes.append(previous_classes[example_index])
            current_gain = self._information_gain(previous_classes, [pos_classes, neg_classes])
            # gain_array.append((current_gain,thresh_array[i],i,pos,neg))
            if max_gain is None or current_gain > max_gain:
                thresh_value = thresh_array[i]
                max_gain = current_gain
                pos_classes_set = pos_classes
                pos_features_set = pos_examples
                neg_classes_set = neg_classes
                neg_features_set = neg_examples
                feature = i

        return max_gain, feature, thresh_value, pos_features_set, pos_classes_set, neg_features_set, neg_classes_set

    def classify(self, features):
        """
        Use the fitted tree to classify a list of examples. Return a list of class labels.
        :param features:  The list of all the features that lead to a classification
        :return: The list of classifications based on the features list.  It should be of the same length.
        """
        class_labels = []
        class_labels = [self.root.decide(feature) for feature in features]

        return class_labels

    def _entropy(self, class_vector):
        """
        Compute the entropy for a list of classes (given as either 0 or 1).
        Found on page 704 in the textbook.
        :param class_vector:
        :return:
        """
        # TODO: finish this
        ratio = 1.0 * sum(class_vector) / len(class_vector)
        return self._B(ratio)

    def _B(self, q):
        """
        Calculates the B(q) value used in entropy and information gain
        Found on page 704 in the textbook
        :return: returns the value of B(q) as stated in class
        """
        # TODO: finish this
        if q < 1 and q > 0:
            return -(q * math.log(q, 2) + (1 - q) * math.log(1 - q, 2))
        return 0.0

    def _information_gain(self, all_classes, split_classes):
        """
        Compute the information gain between the split classifications(each list of 0 and 1 values).
        Found on page 704 in the textbook
        :param all_classes:  a list of all the classifications
        :param split_classes: a list of all the classifications split into lists.  If you have a binary tree then the
          list will have two elements, the true list and false list.
        :return: The information gain value
        """
        # TODO: finish this
        ent = self._entropy(all_classes)
        remainder = 0.0
        previous_classes = list(all_classes)
        # print("current classes = "+str(list(current_classes)))
        # print("prev class = "+str(previous_classes))
        for c in split_classes:
            if len(c) != 0:
                remainder += 1.0 * len(c) / len(previous_classes) * self._B(1.0 * sum(c) / len(c))
        return ent - remainder
